# Use a lightweight Python image as the base
FROM python:3.8.0-slim

# Set the working directory in the container
WORKDIR /app

# Copy the current directory contents into the container at /app
ADD . /app

# Install Flask and other dependencies
RUN pip install --no-cache-dir Flask

# Set environment variable (optional)
ENV NAME=Mark

# Expose the port on which your Flask app runs (assuming app.py uses port 5000)
# EXPOSE 5000

# Define the command to run your Flask application
CMD ["python", "app.py"]
